import os
import sys
import md5 # python 2
#from hashlib import md5 #python 3
import walkfs
import random

"""
1. get total filesize
2. choose random offset
3. traverse files until you reach that number of bytes
4. if the substring would cross the boundary, go to 2
5. if the substring contains a newline, go to 2
6. md5 hash the string, print it, and exit
"""

def getFilesAndSizes(path):
    sizes = []
    for f in walkfs.genTextFileList(path):
        sizes.append((f, os.path.getsize(f)))
    return sizes

def getRandomSubstring(filesizes, total_size, length):
    rand_offset = int(random.random() * total_size)
    curr_size = 0

    for fname, fsize in filesizes:
        if curr_size + fsize > rand_offset:
            """ we have arrived at our file """
            file_offset = rand_offset - curr_size
            with open(fname) as fd:
                fd.seek(file_offset)
                return fd.read(length)
        curr_size += fsize

def validateSubstring(s):
    if len(s) != length: return False
    if "\n" in s: return False
    if "\r" in s: return False
    return True

root = sys.argv[1]
length = int(sys.argv[2])


filesizes = getFilesAndSizes(root)

total_size = sum([ fs[1] for fs in filesizes ])

#print("total text file size: " + str(total_size))

while True:
    substring = getRandomSubstring(filesizes, total_size, length)

    if validateSubstring(substring):
        #print("'"+substring+"'")
        
        target_hash = md5.new(bytearray(substring)).hexdigest() # python 2
        #target_hash = md5(bytes("text","ascii")).hexdigest()   # python 3

        print(target_hash)

        break


#print("done")
