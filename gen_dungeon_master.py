#!/usr/bin/python

# VScode python hello world -- https://code.visualstudio.com/docs/python/python-tutorial
# In-memory zip with python -- https://stackoverflow.com/questions/2463770/python-in-memory-zip-library
# setting zipfile password -- https://stackoverflow.com/questions/43439979/python-zipfile-how-to-set-a-password-for-a-zipfile
# pyminizip (for setting password) -- https://github.com/smihica/pyminizip
#
# PoC ATTEMPT #4
#
# https://stackoverflow.com/questions/2195747/python-code-to-create-a-password-encrypted-zip-file 




import subprocess
import os
import random
import time
import shutil
import string
import zipfile
import sys

# so dungeon. much generate. very exit.
#class DungeonMasterGenerator:

"""
 what it do:
    - creates a ton of zip files (aka "dungeon barriers")
    - creates a "dungeon exit"
    - nests the dungeon barrier zip files into each other
        - creates a randomized unbalanced, and unsorted tree of zips (unbalanced = differing numbers of child nodes)
    - creates file level_info.<key_length> for each "dungeon level"
        - where <key_length> is the length of the string of the zip file password ("unlock key")
    - generates level_info data on the fly (no dataset required -- keeps things light)
        - creates random data of a random length inside the level_info file
        - somewhere inside this data are strings of <key_length> characters which will unlock each barrier on a level
            - no unlock key works for more than one barrier (unique keys for each barrier)
"""

# TODO: turn off 7zip verbose output (make it silent, so user can't watch what's happening)
# TODO: check to see if 7zip is installed before doing anything
# TODO: check to see if there are any other files in the working directory, die hard if there are

# NOTE: availableBarrierIds' pointer is the only variable that lives outside local function scope, besides the instance vars below

# TODO: make these cli arguments
TOTAL_BARRIER_COUNT = 10 # must be greater than MIN_CHILD_NODES

MAX_BARRIER_CHILD_NODES = 10 # must be less than BARRIER_COUNT
MIN_BARRIER_CHILD_NODES = 3 # must be greater than 1

MAX_BARRIER_UNLOCK_KEY_LENGTH = 20
MIN_BARRIER_UNLOCK_KEY_LENGTH = 5

MIN_LEVEL_INFO_DATA_SIZE = 1 # 10000 # number of characters / bytes -- currently 10kB
MAX_LEVEL_INFO_DATA_SIZE = 100 # 2000000 # number of characters / bytes -- currently 2MB


# assign explicit file paths for the individual that will exist in every zip file
# TODO: create file from this script, so i can just distribute the one .py file
levelInfoTextFile = os.path.join(os.getcwd(), 'LEVEL_INFO.txt')
levelInfoTextFileDungeonExit = os.path.join(os.getcwd(), 'DUNGEON_EXIT.txt')




def dependenciesInstalled():
    #requires 7zip
         # $ sudo apt-get update.
         # $ sudo apt-get install p7zip-full
    #requires python 3
    print("NotYetImplemented")
    
    return True




def getCurrentFolderZipFilePath(barrierId):

    return os.path.join(os.getcwd(), str(barrierId)) + ".zip"



def createBarrierUnlockKey(levelInfoFilePath):

    #
    # call dan's generator here -------------------------------------------------->>>>>>>>>>>>>>>>>
    #

    # get file extension (key_length), no period
    searchStringSize = os.path.splitext(levelInfoFilePath)[1].replace('.', '')

    md5GeneratorPath = os.path.join(os.getcwd(), 'gen_rand_md5.py')

    barrierUnlockKey = subprocess.check_output(
        ['python', 'gen_rand_md5.py', os.getcwd(), searchStringSize], universal_newlines=True).split()[0]

    #DEBUG
    print(barrierUnlockKey)
    #time.sleep(5)


    return barrierUnlockKey




def nestRandomBarriers(availableBarrierIds):
    
    #DEBUG
    print("starting nestRandomBarriers()...")

    #DEBUG
    print("len(availableBarrierIds): " + str(len(availableBarrierIds)))

    #pick a parent node from the zipFileIds
    parentBarrierId = random.choice(availableBarrierIds)

    # DEBUG
    print("parentBarrierId: " + parentBarrierId)

    # parentBarrierId is not longer available to be a child in this function run
    availableBarrierIds.remove(parentBarrierId)

    # DEBUG
    print("len(availableBarrierIds): " + str(len(availableBarrierIds)))

    # create a list to hold childBarrierIds (chosen randomly)
    childBarrierIds = []

    # make sure we don't choose a range larger than what's available
    maxBarrierChildNodes = MAX_BARRIER_CHILD_NODES if len(availableBarrierIds) >= MAX_BARRIER_CHILD_NODES else len(availableBarrierIds)

    # make sure we don't choose a minimum higher than what's available (absolute minimum child nodes is 1)
    minBarrierChildNodes = MIN_BARRIER_CHILD_NODES if len(availableBarrierIds) > MIN_BARRIER_CHILD_NODES else 0
    
    # DEBUG
    print("minChildNodes: " + str(minBarrierChildNodes))
    print("maxChildNodes: " + str(maxBarrierChildNodes))

    #time.sleep(5)
    print(availableBarrierIds)

    # create a range between MIN and rand(MAX)
    childBarrierIdRange = range(minBarrierChildNodes, random.randint(minBarrierChildNodes, maxBarrierChildNodes), 1)

    # choose a random number of child nodes (0 - MAX_CHILD)
    for n in childBarrierIdRange:

        childBarrierId = random.choice(availableBarrierIds)

        childBarrierIds.append(childBarrierId)

        # DEBUG
        print("childBarrierId: " + childBarrierId)
        #time.sleep(1)

        availableBarrierIds.remove(childBarrierId)

    # create LEVEL_INFO file (inside the dungeon level)
    print("creating LEVEL_INFO file...")
    levelInfoFilePath = createLevelInfoFile(parentBarrierId)

    # add the child zips to the parent
    for childBarrierId in childBarrierIds:

        # createBarrierUnlockKey
        barrierUnlockKey = createBarrierUnlockKey(levelInfoFilePath)

	print("unlock key created")

        # update zip password with md5 from relevant LEVEL_INFO dataset
        updateBarrierUnlockKey(childBarrierId, barrierUnlockKey)

	print("barrier unlock key updated")

        # add the child zip to the parent zip
        subprocess.call(['7z', 'a', '-p123456', '-y', getCurrentFolderZipFilePath(parentBarrierId), getCurrentFolderZipFilePath(childBarrierId)])

        # remove the childBarrierId zip file from the file system
        os.remove(getCurrentFolderZipFilePath(childBarrierId))

    # parentBarrierId is now available to be a child in subsequent function runs
    availableBarrierIds.append(parentBarrierId)

    # remove LEVEL_INFO file from filesystem (it's in the archive / dungeon level now)
    os.remove(levelInfoFilePath)




def removeExistingLevelInfoFile(barrierId):
    """
    if a LEVEL_INFO file exists, remove it from the archive
    --- due to the random way zip files are nested, it's entirely
        possible that the zip archive may already have nested files in it
        and thusly, an existing LEVEL_INFO.<key_length>, which simply
        will not do since there can only be one dataset per dungeon
        level with the current design.
    """
    targetZipFile = zipfile.ZipFile(getCurrentFolderZipFilePath(barrierId))
    zipFileContents = targetZipFile.namelist()
    targetZipFile.close()

    # DEBUG
    print(len(zipFileContents))

    # DEBUG
    if (len(zipFileContents) != 0):
        print("zipFileContents: " + zipFileContents[0])

    # if the archiveFile contains LEVEL_INFO, then remove it from the archive
    for archiveFile in zipFileContents:
        print("archiveFile: " + archiveFile)
        if "LEVEL_INFO" in archiveFile :
            subprocess.call(['7z', 'd', '-p123456', '-y', getCurrentFolderZipFilePath(barrierId), archiveFile])
            print("removed " + archiveFile)





def createLevelInfoFile(barrierId):

    removeExistingLevelInfoFile(barrierId)

    # pick a random key length
    barrierUnlockKeyLength = random.randint(MIN_BARRIER_UNLOCK_KEY_LENGTH, MAX_BARRIER_UNLOCK_KEY_LENGTH)

    # get levelInfoFilePath
    levelInfoFilePath = os.path.join(os.getcwd(), 'LEVEL_INFO.' + str(barrierUnlockKeyLength))

    # create a random size for the LEVEL_INFO file data, within user constraints
    randomLevelInfoDataSize = random.randint(MIN_LEVEL_INFO_DATA_SIZE, MAX_LEVEL_INFO_DATA_SIZE)

    # create data that goes inside the LEVEL_INFO file (random characters)
    levelInfoData = ''
    for i in range(randomLevelInfoDataSize):
        levelInfoData += levelInfoData.join(random.choice(string.ascii_letters))

    # create file -- level_info.<key_length> and write to parentBarrierId
    # write to the file and close it
    with open(levelInfoFilePath, 'w+') as levelInfoFile:
        levelInfoFile.write(levelInfoData)

    # add the LEVEL_INFO file to the dungeon level
    subprocess.call(['7z', 'a', '-p123456', '-y', getCurrentFolderZipFilePath(barrierId), levelInfoFilePath])

    return levelInfoFilePath




def displayDungeonExitMd5():
    # md5 of dungeonExitPath to be displayed to user at program exit, when generation is finished
    # this is to allow confirmation of a challenge-complete claim
    dungeonExitPath = ""
    
    print("NotYetImplemented")




def createDungeonEntrance():

    # should be one zip file left, which we'll rename to DUNGEON_ENTRANCE.zip, so let's get the path
    files = os.listdir(os.curdir)

    targetFilePath = ""

    dungeonEntranceBarrierId = ""

    # parse all files in current folder, looking for a .zip
    for n in files:
        if ('.zip' in n):
            targetFilePath = n
            dungeonEntranceBarrierId = n.split('.')[0]

    renamedFilePath = getCurrentFolderZipFilePath('DUNGEON_ENTRANCE')

    # rename the file
    os.rename(targetFilePath, renamedFilePath)

    return dungeonEntranceBarrierId




def createBarriers(barrierCount):
    """
    create a shitton of zip files, containing default LEVEL_INFO.txt
    zipFileId is simply an integer, which becomes the name of the zipFile on the fileystem (for now)
    eventually, zipFileId should be something like, "Sheet_Metal_Door"
    """

    barrierIds = []

    for barrierId in range(barrierCount):

        #add the barrierId to the barrierIds list
        barrierIds.append(str(barrierId))
        
        #create the zipFile
        subprocess.call(['7z', 'a', '-p123456', '-y', getCurrentFolderZipFilePath(barrierId), levelInfoTextFile])

    return barrierIds




def addDungeonExitToRandomBarrier(availableBarrierIds, dungeonExitBarrierId):
    """
    make sure that the dungeon exit is at the bottom of the tree nodes
    we do this by adding it to an availableBarrierId, so it has the to be shuffled down by iterations of randomness
    """

    dungeonExitParentBarrierId = random.choice(availableBarrierIds)

    subprocess.call(['7z', 'a', '-p123456', '-y',
        getCurrentFolderZipFilePath(dungeonExitParentBarrierId), getCurrentFolderZipFilePath(dungeonExitBarrierId)])

    print("dungeonExitParentBarrierId: " + dungeonExitParentBarrierId)
    print("dungeonExitBarrierId: " + dungeonExitBarrierId)

    # remove it from the filesystem
    os.remove(getCurrentFolderZipFilePath(dungeonExitBarrierId))

    #time.sleep(5)




def createDungeonExit(availableBarrierIds):

    dungeonExitBarrierId = random.choice(availableBarrierIds)

    # remove the dungeonExitId from availableBarrierIds
    availableBarrierIds.remove(dungeonExitBarrierId)

    # add the levelInfoTextFileDungeonExit to the zipFileDungeonExit
    subprocess.call(['7z', 'a', '-p123456', '-y',
        getCurrentFolderZipFilePath(dungeonExitBarrierId), levelInfoTextFileDungeonExit])

    return dungeonExitBarrierId




def generateMd5FromTextFile(characterCount, textFilePath):

    print("NotYetImplemented")




def createLevelInfoData():

    print("NotYetImplemented")




def updateBarrierUnlockKey(barrierId, newPassword):
    """
    ya can't change a password on a zip archive without 
    extracting everything and creating a new archive with the
    same contents while setting a different password...
        - then delete the temporarily extracted contents
    """

    TEMP_FOLDER_NAME = 'temp'

    tempFolderPath = os.path.join(os.getcwd(), TEMP_FOLDER_NAME)

    # first, make a temporary working folder in the current directory
    os.makedirs(tempFolderPath)
    
    # get the full path for the zip we'll be working with
    targetZipFile = getCurrentFolderZipFilePath(barrierId)

    # extract the zip with password 123456 to new temporary subfolder
    subprocess.call(['7z', 'e', '-p123456', '-o' + TEMP_FOLDER_NAME, targetZipFile])

    # delete the archive with the old password
    os.remove(targetZipFile)

    # re-archive with new password, target everything in the temp folder
    subprocess.call(['7z', 'a', '-p' + newPassword, '-y', targetZipFile, './' + TEMP_FOLDER_NAME + '/*'])

    # remove temp folder and contents
    shutil.rmtree(tempFolderPath)




def main():
    
    # TODO: check for existing zip files, if existing, die hard
    print("WARNING! Run DungeonMasterGenerator.py in its own subfolder, and make sure there is nothing else in it.")

    # create barrier zipfiles and store their filename to barrierIds list
    availableBarrierIds = createBarriers(TOTAL_BARRIER_COUNT)

    # create a dungeon exit in one of the barriers
    dungeonExitBarrierId = createDungeonExit(availableBarrierIds)

    # add it to a random barrier
    addDungeonExitToRandomBarrier(availableBarrierIds, dungeonExitBarrierId)

    # hide that shit -- each time this runs it creates a new dungeon level
    while (len(availableBarrierIds) > 1):

        print("len(availableBarrierIds): " + str(len(availableBarrierIds)))

        nestRandomBarriers(availableBarrierIds)



    # create the dungeon entrance from the one leftover .zip file
    dungeonEntranceBarrierId = createDungeonEntrance()

    # DEBUG
    print("\n dungeonExitBarrierId: " + dungeonExitBarrierId)
    print("\n dungeonEntranceBarrierId: " + dungeonEntranceBarrierId)

    updateBarrierUnlockKey('DUNGEON_ENTRANCE', "derp5")

    for i in range(5):
        print(''.join(random.choice(string.ascii_letters)))

    print("=========COMPLETE=========")




if __name__ == '__main__':
    main()
