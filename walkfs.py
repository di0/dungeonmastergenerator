import os

def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

# in the dungeon master challenge, only care about numbered file extensions
def genTextFileList(root):
    for root, dirs, files in os.walk(root):
        for fname in files:
            fileExtension = os.path.splitext(fname)[1].replace(".", "")
            if (fname[-4:] == ".txt" or isInt(fileExtension)):
                path = root + "/" + fname
                yield path

if __name__ == "__main__":
    for path in genTextFileList("/media/pi/30CC-5641"):
        print(path)
